import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import FileInput from "../UI/Form/FileInput";
import FormElement from "../UI/Form/FormElement";


const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
}));

const AddNewPhotoForm = ({onSubmit}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        title: "",
        image: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({...prevState, [name]: file}));
    };


    return (
        <form
            className={classes.root}
            noValidate
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <FormElement
                name="title"
                label="Product title"
                required={true}
                value={state.title}
                onChange={inputChangeHandler}
            />
            <FormControl fullWidth className={classes.margin} variant="outlined">
                <FileInput
                    label="Image"
                    name="image"
                    onChange={fileChangeHandler}
                />
            </FormControl>
            <FormControl fullWidth className={classes.margin} variant="outlined">
                <Button type="submit" color="primary">Create</Button>
            </FormControl>
        </form>
    );
};

export default AddNewPhotoForm;