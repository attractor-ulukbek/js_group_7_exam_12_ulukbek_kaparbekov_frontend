import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Container} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../store/actions/usersActions";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {Link as RouterLink} from "react-router-dom";
import Link from "@material-ui/core/Link";



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

const Layout = (props) => {

    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const logout = () => {
        dispatch(logoutUser());
    };


    const classes = useStyles();

    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="static">
                <Container>
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            <Link component={RouterLink} color={"inherit"} to={'/'}>PhotoGallery</Link>
                        </Typography>


                        <Button component={RouterLink} color={"inherit"} to={user ? '/users/'+user._id : '/'}>
                             {user && 'Hello, ' + user.displayName}
                        </Button>
                        <Button
                            aria-controls="simple-menu"
                            aria-haspopup="true"
                            onClick={handleClick}
                            color={"inherit"}
                        >
                             {user ? "menu" : "Login"}
                        </Button>
                        <Menu
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem><Link component={RouterLink} color={"inherit"} to={'/register'}>Register</Link></MenuItem>
                            {user && <MenuItem><Link onClick={logout} component={RouterLink} color={"inherit"} to={'/login'} >Login</Link></MenuItem>}
                            {!user && <MenuItem><Link component={RouterLink} color={"inherit"} to={'/login'} >Login</Link></MenuItem>}
                            {user && <MenuItem onClick={logout}>Logout</MenuItem>}

                        </Menu>
                    </Toolbar>
                </Container>
            </AppBar>

            <div>
                {props.children}
            </div>
        </>

    );
};

export default Layout;