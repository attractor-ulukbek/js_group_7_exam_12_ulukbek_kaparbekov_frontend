import React from "react";
import {TextField, MenuItem} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const FormElement = (props) => {
    let inputChildren = null;
    if (props.options) {
        inputChildren = props.options.map((option) => (
            <MenuItem key={option._id} value={option._id}>
                {option.title}
            </MenuItem>
        ))
    }
    return (
        <Grid item xs={12}>
            <TextField
                variant="outlined"
                fullWidth
                select={props.select}
                required={props.required}
                error={!!props.error}
                helperText={props.error}
                id={props.name}
                label={props.label}
                type={props.type}
                name={props.name}
                value={props.value}
                onChange={props.onChange}
                autoComplete={props.name}
                multiline={props.multiline}
                rows={props.rows}
            >
                {inputChildren}
            </TextField>
        </Grid>
    );
};


export default FormElement;