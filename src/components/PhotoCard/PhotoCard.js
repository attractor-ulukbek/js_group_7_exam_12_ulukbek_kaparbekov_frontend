import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";
import {apiURL} from "../../constants";
import {useDispatch} from "react-redux";
import {deletePhotos} from "../../store/actions/photosActions";

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
    },
    margin: {
        paddingTop: theme.spacing(4),
    }
}));

export default function PhotoCard(props) {
    const classes = useStyles();

    const dispatch = useDispatch();

    const deleteCurrentPhoto = (id) => {
        dispatch(deletePhotos(id))
    }

    let cardImage = apiURL + "/uploads/" + props.photo;

    return (
        <Grid item xs={4} className={classes.margin}>
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        height="140"
                        image={cardImage}
                        title="Contemplative Reptile"
                        onClick={props.onClick}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.title}
                        </Typography>

                    </CardContent>
                </CardActionArea>
                <CardActions>
                    {props.owner ? <Button onClick={() => deleteCurrentPhoto(props.photoId)} size="small" color="primary">
                        Delete
                    </Button> : <Button onClick={props.onCreatorClick} size="small" color="primary">
                        {`by: ${props.creator}`}
                    </Button>}


                </CardActions>
            </Card>
        </Grid>
    );
}