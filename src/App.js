import React from 'react';
import Layout from "./components/Layout/Layout";
import {Container} from "@material-ui/core";
import Routes from "./Routes";
import {Switch} from "react-router-dom";


function App() {
    return (
        <div className="App">
            <Switch>
                <Layout>
                    <Container>
                        <Routes/>
                    </Container>
                </Layout>
            </Switch>
        </div>
    );
}

export default App;