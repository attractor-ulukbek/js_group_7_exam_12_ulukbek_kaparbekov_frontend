import React, {useEffect} from 'react';
import {Button, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchDesirePhotos} from "../../store/actions/photosActions";
import PhotoCard from "../../components/PhotoCard/PhotoCard";
import {Link as RouterLink} from "react-router-dom";


const ViewProfile = (props) => {

    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const photos = useSelector(state => state.photos.desire_photos);

    useEffect(() => {
        dispatch(fetchDesirePhotos(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    let thisMyProfile = false;

    if (user && user._id === props.match.params.id) {
        thisMyProfile = true;
    }

    return (
        <>
            <Grid container justify={"space-around"} alignItems={"center"}>
                <Grid item xs={3}><h1>{photos[0] && photos[0].creator.displayName}'s gallery</h1></Grid>
                <Grid item xs={2}>
                    {thisMyProfile && <Button component={RouterLink} to={'/add-new-photo'} vanriant="contained" color="primary">add new photo</Button>}
                </Grid>
            </Grid>
            <Grid container>
                {photos.map((v) => {
                    return <PhotoCard
                        key={v._id}
                        title={v.title}
                        creator={v.creator.displayName}
                        photo={v.image}
                        photoId={v._id}
                        owner={thisMyProfile}
                    />
                })}
            </Grid>

        </>
    );
};

export default ViewProfile;