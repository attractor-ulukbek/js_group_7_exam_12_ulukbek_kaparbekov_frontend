import React from "react";
import {useDispatch} from "react-redux";
import AddNewPhotoForm from "../../components/AddNewPhotoForm/AddNewPhotoForm";
import {createPhoto} from "../../store/actions/photosActions";

const AddNewPhoto = () => {
    const dispatch = useDispatch();

    const creatingPhoto = data => {
        dispatch(createPhoto(data));
    };

    return (
        <>
            <h1>New photo</h1>
            <AddNewPhotoForm
                onSubmit={creatingPhoto}
            />
        </>
    );
};

export default AddNewPhoto;