import React, {useEffect, useState} from 'react';
import PhotoCard from "../../components/PhotoCard/PhotoCard";
import TransitionsModal from "../../components/ModalWindow/ModalWindow";
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/actions/photosActions";
import {apiURL} from "../../constants";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

const AllPhotos = (props) => {

    const [open, setOpen] = useState(false);
    const [windowContent, setWindowContent] = useState(<h1>No content</h1>);

    const handleOpenWindow = (url) => {
        setWindowContent(url)
        setOpen(true);
    };

    const handleCloseWindow = () => {
        setOpen(false);
    };

    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);

    useEffect(() => {
        dispatch(fetchPhotos());
    }, [dispatch]);

    const onCreatorClick = (id) => {
        props.history.push('/users/' + id)
    }
    return (
        <div>
            <h1>All Photos</h1>
            <Grid container>
                {photos.map((v) => {
                    return <PhotoCard
                        key={v._id}
                        title={v.title}
                        creator={v.creator.displayName}
                        photo={v.image}
                        onClick={() => handleOpenWindow(v.image)}
                        onCreatorClick={() => onCreatorClick(v.creator._id)}
                    />
                })}
            </Grid>
            <TransitionsModal
                open={open}
                handleOpen={handleOpenWindow}
                handleClose={handleCloseWindow}
                children={<Card>
                    <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        height="500"
                        image={apiURL + "/uploads/" + windowContent}
                        title="Contemplative Reptile"
                    />
                    <CardActions>
                        <Button onClick={handleCloseWindow} size="small" color="primary">
                            CLOSE
                        </Button>
                    </CardActions>
                </Card>}
            />
        </div>
    );
};


export default AllPhotos;