import React from "react";
import {Route} from "react-router-dom";
import AllPhotos from "./containers/AllPhotos/AllPhotos";
import ViewProfile from "./containers/ViewProfile/ViewProfile";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AddNewPhoto from "./containers/AddNewPhoto/AddNewPhoto";

const Routes = () => {
    return (
            <>
                <Route path="/" exact component={AllPhotos}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/register" exact component={Register}/>
                <Route path="/users/:id" exact component={ViewProfile}/>
                <Route path="/add-new-photo" exact component={AddNewPhoto}/>
            </>
    );
};

export default Routes;