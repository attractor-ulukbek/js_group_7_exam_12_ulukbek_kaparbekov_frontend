import {FETCH_DESIRE_PHOTOS_SUCCESS, FETCH_PHOTOS_SUCCESS} from "../actionTypes";

const initialState = {
    photos: [],
    desire_photos: []
};

const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.photos};
        case FETCH_DESIRE_PHOTOS_SUCCESS:
            return {...state, desire_photos: action.photos};
        default:
            return state;
    }
};

export default photosReducer;