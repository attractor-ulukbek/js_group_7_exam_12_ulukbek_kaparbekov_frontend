import {push} from "connected-react-router";
import axios from "../../axiosApi";
import {FETCH_DESIRE_PHOTOS_SUCCESS, CREATE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS, DELETE_PHOTO_SUCCESS} from "../actionTypes";

const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};

const deletePhotoSuccess = () => {
    return {type: DELETE_PHOTO_SUCCESS};
};
const fetchDesirePhotosSuccess = photos => {
    return {type: FETCH_DESIRE_PHOTOS_SUCCESS, photos};
};
const createPhotoSuccess = () => {
    return {type: CREATE_PHOTO_SUCCESS};
};

export const fetchPhotos = () => {
    return async dispatch => {
        const data = await axios.get("/photos").then(response => {
            dispatch(fetchPhotosSuccess(response.data));
        });
        return data;
    };
};

export const deletePhotos = (id) => {
    return async dispatch => {
        const data = await axios.delete("/photos/" + id).then(response => {
            dispatch(deletePhotoSuccess(response.data));
            dispatch(push("/"));
        });
        return data;
    };
};

export const fetchDesirePhotos = (creatorId) => {
    return async dispatch => {
        return await axios.get("/photos?creator=" + creatorId).then(response => {
            dispatch(fetchDesirePhotosSuccess(response.data));
        });
    };
};

export const createPhoto = photosData => {
    return async dispatch => {
        await axios.post("/photos", photosData);
        dispatch(createPhotoSuccess());
        dispatch(push("/"));
    };
};